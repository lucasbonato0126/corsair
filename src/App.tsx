import { useEffect, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import { Main, Game } from "./pages";
import { AudioPlayer } from "./components/AudioPlayer";

const ThemeSongs = {
  main: "../../public/assets/sounds/tracks/destination-tortuga.mp3",
  game: "../../public/assets/sounds/tracks/seafights-groove-addicts.mp3",
};

export const App = () => {
  const [audioUrl, setAudioUrl] = useState("");
  const location = useLocation();

  useEffect(() => {
    const currentPath = location.pathname;
    console.log({ currentPath });

    if (currentPath === "/") {
      setAudioUrl(ThemeSongs.main);
    } else if (currentPath === "/game") {
      setAudioUrl(ThemeSongs.game);
    }
  }, [location.pathname]);

  return (
    <>
      <AudioPlayer audioUrl={audioUrl} />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/game" element={<Game />} />
      </Routes>
    </>
  );
};
