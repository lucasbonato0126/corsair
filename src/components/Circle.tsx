import { useEffect, useRef } from 'react';
import { BufferGeometry, Vector3 } from 'three';

import { RADIUS } from "../constants/game";

const CIRCLE_SEGMENT_COUNT = 64;

export function Circle() {
    const bufferGeometryRef = useRef<BufferGeometry>(null);

    useEffect(() => {
        const points = [];
        
        for (let i = 0; i <= CIRCLE_SEGMENT_COUNT; i++) {
            const theta = (i / CIRCLE_SEGMENT_COUNT) * Math.PI * 2;
            points.push(new Vector3(Math.cos(theta) * RADIUS, Math.sin(theta) * RADIUS, 0));
        } 

        bufferGeometryRef.current?.setFromPoints(points);
    }, [])
    
    return (
        <bufferGeometry ref={bufferGeometryRef}>
          <lineBasicMaterial attach="material" color={0xFFFFFF} opacity={0.1} transparent />
        </bufferGeometry>
    );
}