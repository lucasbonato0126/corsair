import { useFrame, useLoader } from "@react-three/fiber"
import { TextureLoader, Vector2 } from "three"
import { COINS, RADIUS } from "../constants/game";
import { useShipStore } from "./Ship";
import { detectCollision } from "../utils/helpers";
import { create } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";
import { shallow } from "zustand/shallow";
import { useGameStore } from "./GameScene/useLogic";

type Coin = {
    angle: number;
    radius: number;
    size: number;
    collected: boolean;
};

type CoinStore = {
    coins: Coin[];
    hasCollectAllCoins: boolean;
    updateCoinAsCollected: (index: number) => void;
    regenerateCoins: () => void;
};

const generateDefaultCoins = () => new Array(COINS)
    .fill(null)
    .map((_, i) => ({
        angle: ((Math.PI * 2) / COINS) * i,
        radius: RADIUS,
        size: 1,
        collected: false,
    }))
    .filter(({ angle }) => Math.PI * 0.5 !== angle);

export const useCoinsStore = create(subscribeWithSelector<CoinStore>((set) => ({
    coins: generateDefaultCoins(),
    hasCollectAllCoins: false,
    updateCoinAsCollected: (index) => set((state) => {
        const newCoins = [...state.coins];

        newCoins[index].collected = true;
        const hasCollectAllCoins = newCoins.every(({ collected }) => collected);

        return { coins: newCoins, hasCollectAllCoins };
    }),
    regenerateCoins: () => set(() => ({ coins: generateDefaultCoins(), hasCollectAllCoins: false })),
})));

useCoinsStore.subscribe(
    (state) => state.hasCollectAllCoins,
    (hasCollectAllCoins) => {
        if (!hasCollectAllCoins) return;
        
        useShipStore.getState().reset();
        useCoinsStore.getState().regenerateCoins();
        useGameStore.getState().goToNextStage();
    },
    {
        equalityFn: shallow,
    }
);

export function Coins() {
    const colorMap = useLoader(TextureLoader, 'assets/nova_moeda.png');
    const { coins, updateCoinAsCollected } = useCoinsStore((state) => ({ coins: state.coins, updateCoinAsCollected: state.updateCoinAsCollected }));
    const addToScore = useGameStore((state) => state.addToScore);

    useFrame((_, delta) => {
        const { angle, speed, direction, size } = useShipStore.getState();

        const playerAngle = angle;
        const playerSpeed = delta * direction * speed;
        const playerSize = size * Math.PI / 360;
        let shouldUpdate = false;

        coins.forEach((coin, i) => {
            const { collected, size: coinSizeUnits, angle: coinAngle } = coin;

            if (collected) return;
            
            const coinSpeed = 0;
            const coinSize = coinSizeUnits * Math.PI / 180;

            const collision = detectCollision(
                new Vector2(playerAngle, 0), new Vector2(playerSpeed, 0), playerSize,
                new Vector2(coinAngle, 0), new Vector2(coinSpeed, 0), coinSize,
                4
            );

            shouldUpdate = shouldUpdate || collision;

            if (!collision) return;

            addToScore(1);
            updateCoinAsCollected(i);
        });
    });

    return (
        <group>
            {coins.map(({ angle, collected, radius, size }, i) => !collected && (
                <mesh
                    key={i}
                    position={[radius * Math.cos(angle), radius * Math.sin(angle), 0]}
                    rotation={[Math.PI / 2, 0, 0]}
                    scale={[size, size, size]}
                    castShadow
                >
                    <cylinderGeometry args={[2, 2, 0.5, 16]} />
                    <meshPhongMaterial map={colorMap} />
                </mesh>
            ))}
        </group>
    );
}