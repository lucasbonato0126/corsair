import { extend } from '@react-three/fiber';
import { BufferGeometry } from 'three';
import { useLogic } from './useLogic';
import { Cannonballs } from '../Cannonballs';
import { Coins } from '../Coins';
import { Ship } from '../Ship';
import { Water } from '../Water';
import { Circle } from '../Circle';
import { Island } from '../Island';

extend({ CircleBufferGeometry: BufferGeometry })

export function GameScene() {
    const {
        ambientLightRef,
        directionalLightRef,
    } = useLogic();

    return (
       <>
            <ambientLight ref={ambientLightRef} color={0xffffff} intensity={0.35} />
            <hemisphereLight color={0xffffff} groundColor={0xffffff} intensity={0.5} position={[0, 0, 500]} />
            <directionalLight
                ref={directionalLightRef}
                color={0xffffff}
                intensity={1}
                position={[-1, 1, 1]}
                castShadow={true}
                shadow-mapSize-width={1024}
                shadow-mapSize-height={1024}
                shadow-camera-left={-50}
                shadow-camera-right={50}
                shadow-camera-top={50}
                shadow-camera-bottom={-50}
                shadow-camera-near={1}
                shadow-camera-far={200}
            />
            <Circle />
            <Ship />
            <Coins />
            <Island />
            <Cannonballs />
            <Water />
        </>
    )
}