import { useRef, useEffect } from "react";
import { useThree } from "@react-three/fiber";
import { AmbientLight, HemisphereLight, DirectionalLight } from "three";
import { useShipStore } from "../Ship";
import { subscribeWithSelector } from "zustand/middleware";
import { create } from "zustand";
import { useCoinsStore } from "../Coins";
import { PerspectiveCamera } from "three";

interface IGameStore {
  stage: number;
  score: number;
  record: number;
  reset: () => void;
  goToNextStage: () => void;
  addToScore: (score: number) => void;
}

export const useGameStore = create(
  subscribeWithSelector<IGameStore>((set) => ({
    stage: 1,
    score: 0,
    record: 0,
    reset: () => {
      set({ stage: 1, score: 0 });
      useCoinsStore.getState().regenerateCoins();
      useShipStore.getState().reset();
    },
    goToNextStage: () => set((state) => ({ stage: state.stage + 1 })),
    addToScore: (score: number) => {
      const coin_sound = new Audio("/assets/sounds/coin_sound.wav");
      coin_sound.play();

      set((state) => {
        const newScore = state.score + score;
        const newRecord = Math.max(newScore, state.record);

        return { score: newScore, record: newRecord };
      });
    },
  }))
);

export function useLogic() {
  const ambientLightRef = useRef<AmbientLight>(null);
  const hemisphereLightRef = useRef<HemisphereLight>(null);
  const directionalLightRef = useRef<DirectionalLight>(null);
  const { camera, scene, gl } = useThree();
  const perspectiveCamera = camera as PerspectiveCamera;

  useEffect(() => {
    camera.position.z = 100;
  }, [camera]);

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => {
        perspectiveCamera.aspect = window.innerWidth / window.innerHeight;
        perspectiveCamera.updateProjectionMatrix();
        gl.setSize(window.innerWidth, window.innerHeight);
      },
      false
    );
  }, [perspectiveCamera, gl]);

  useEffect(() => {
    ambientLightRef.current?.color.setHSL(0.1, 1, 0.95);

    hemisphereLightRef.current?.color.setHSL(0.6, 1, 0.95);
    hemisphereLightRef.current?.groundColor.setHSL(0.095, 1, 0.75);

    directionalLightRef.current?.color.setHSL(0.1, 1, 0.95);
    directionalLightRef.current?.position.multiplyScalar(50);
  }, [scene]);

  useEffect(() => {
    document.addEventListener("keydown", (e) => {
      const { shipDestroyed, togglePlayerDirection } = useShipStore.getState();
      e.key === " " && !shipDestroyed && togglePlayerDirection();
    });
    return () => document.removeEventListener("keydown", () => {});
  }, []);

  return {
    ambientLightRef,
    directionalLightRef,
  };
}
