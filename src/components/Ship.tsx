import { useFrame, useLoader } from "@react-three/fiber";
import { useCallback, useEffect, useRef } from "react";
import { DoubleSide, Mesh } from "three";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader.js";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader.js";
import { polarToCartesian } from "../utils/helpers";
import { calculatePlayerSpeed } from "../utils/game";
import { subscribeWithSelector } from "zustand/middleware";
import { create } from "zustand";

type IShipStore = {
  radius: number;
  direction: number;
  size: number;
  speed: number;
  angle: number;
  shipDestroyed: boolean;
  hasStarted: boolean;
  shouldResetPosition: boolean;
  reset: () => void;
  togglePlayerDirection: () => void;
};

const defaultShipValues = {
  shipDestroyed: false,
  hasStarted: false,
  direction: -1,
  size: 6,
  speed: calculatePlayerSpeed(1),
  angle: Math.PI * 0.5,
};

export const useShipStore = create(
  subscribeWithSelector<IShipStore>((set) => ({
    ...defaultShipValues,
    radius: 50,
    shouldResetPosition: false,
    reset: () => set({ ...defaultShipValues, shouldResetPosition: true }),
    togglePlayerDirection: () =>
      set((state) => ({
        hasStarted: true,
        ...(state.hasStarted ? { direction: state.direction * -1 } : {}),
      })),
  }))
);

export function Ship() {
  const shipMtl = useLoader(MTLLoader, "/assets/ship.mtl");
  const shipObject = useLoader(OBJLoader, "/assets/ship.obj", (loader) => {
    shipMtl.preload();
    loader.setMaterials(shipMtl);
  });
  const shipRef = useRef<Mesh>(null);
  const { shipDestroyed, speed, direction, radius, shouldResetPosition } =
    useShipStore((state) => ({
      shipDestroyed: state.shipDestroyed,
      speed: state.speed,
      direction: state.direction,
      radius: state.radius,
      shouldResetPosition: state.shouldResetPosition,
    }));

  const setShipPosition = useCallback(
    (oldAngle: number, delta: number) => {
      const position = oldAngle + delta * 1000 * direction * speed;
      const angle = (position + Math.PI * 2) % (Math.PI * 2);

      if (!shipRef.current) return;

      const { x, y } = polarToCartesian(angle, radius);

      shipRef.current.rotation.y = angle - (direction < 0 ? 0 : Math.PI);
      shipRef.current.position.x = x;
      shipRef.current.position.y = y;
      useShipStore.setState({ angle });
    },
    [direction, radius, speed]
  );

  useEffect(() => {
    const shipStore = useShipStore.getState();

    setShipPosition(shipStore.angle, 0);

    shipRef.current?.traverse((node) => {
      if (node instanceof Mesh) {
        node.castShadow = true;
        if (node.material) {
          node.material.side = DoubleSide;
        }
      }
    });
  }, [setShipPosition]);

  useEffect(() => {
    const shipStore = useShipStore.getState();

    shouldResetPosition && setShipPosition(shipStore.angle, 0);
    useShipStore.setState({ shouldResetPosition: false });
  }, [setShipPosition, shouldResetPosition]);

  useFrame((_, delta) => {
    const shipStore = useShipStore.getState();

    if (!shipStore.hasStarted || shipDestroyed) return;

    setShipPosition(shipStore.angle, delta);
  });

  return (
    <mesh
      ref={shipRef}
      scale={[12, 12, 12]}
      rotation={[Math.PI / 2, Math.PI, 0]}
      castShadow
    >
      <primitive object={shipObject} />
    </mesh>
  );
}
