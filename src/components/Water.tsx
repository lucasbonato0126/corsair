export function Water() {
    return (
        <mesh receiveShadow>
            <planeGeometry attach="geometry" args={[500, 500]} />
            <shadowMaterial opacity={0.25} />
        </mesh>
    );
}