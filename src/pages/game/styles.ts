import styled from "styled-components";

export const Game = styled.div`
  display: flex;
  height: 100vh;
`;

export const Welcome = styled.div`
  position: absolute;
  color: #313133;
  align-items: center;
  text-align: center;
  top: 10px;
  left: 490px;
`;

export const WelcomeTitle = styled.h2`
  font-size: 2rem;
`;

export const GameStatus = styled.div`
  position: absolute;
  top: 1rem;
  right: 2rem;
`;

export const Score = styled.div`
  color: #fff;
  font-size: 3rem;
`;

export const Record = styled.div`
  color: #fff;
  font-size: 3rem;
`;

export const GameDescription = styled.div`
  display: flex;
  flex-direction: column;
  color: #fff;
  position: absolute;
  top: 1rem;
  left: 1rem;
  font-size: 1.5rem;

  p {
    font-size: 2rem;
    margin: 0;
  }
`;
