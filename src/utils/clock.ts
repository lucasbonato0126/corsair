import { create } from 'zustand';
import { useRef, useEffect } from 'react';

type ClockState = {
    time: number;
    delta: number;
    setTimeDelta: (time: number, delta: number) => void;
};

export const useClockStore = create<ClockState>((set) => ({
    time: performance.now(),
    delta: 0,
    setTimeDelta: (time, delta) => set({ time, delta })
}));

export function useClock() {
    const { setTimeDelta } = useClockStore();
    const lastTimeRef = useRef(performance.now());

    useEffect(() => {
        function animate() {
            const currentTime = performance.now();
            const delta = currentTime - lastTimeRef.current;
            setTimeDelta(currentTime, delta);
            lastTimeRef.current = currentTime;
        }

        animate();

        return () => {
            // Cleanup if necessary
        };
    }, [setTimeDelta]);
}